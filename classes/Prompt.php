<?php
/*
 * This file is part of the Bidvest Assessment.
 *
 * @author     Koketso Mabuela <glenton92@gmail.com>
 */
class Prompt
{
    public static function promptIdMessage(): string
    {
        return 'Enter id: ';
    }

    public static function promptNameMessage(string $name = null): string
    {
        return is_null($name) ? 'Enter name: ' : "Enter name [$name]: ";
    }

    public static function promptSurnameMessage(string $surname = null): string
    {
        return is_null($surname) ? 'Enter surname: ' : "Enter surname [$surname]: ";
    }

    public static function promptAgeMessage(int $age = null): string
    {
        return is_null($age) ? 'Enter age: ' : "Enter age [$age]: ";
    }

    public static function promptCurriculumMessage(string $curriculum = null): string
    {
        return is_null($curriculum) ? 'Enter curriculum: ' : "Enter curriculum [$curriculum]: ";
    }

    public static function promptSearchMessage(): string
    {
        return 'Enter search criteria, <keyword>=<value> e.g. name=koketso or age=20'.PHP_EOL. 'or' .PHP_EOL.'Press Enter to retrieve all students';
    }

    public static function promptExitMessage(): string
    {
        return 'ENTER Ctlr + C TO EXIT THE APPLICATION AT ANY TIME.'.PHP_EOL;
    }
}