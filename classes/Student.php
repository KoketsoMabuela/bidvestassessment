<?php
/*
 * This file is part of the Bidvest Assessment.
 *
 * @author     Koketso Mabuela <glenton92@gmail.com>
 */
class Student
{
    private int $id;
    private string $name;
    private string $surname;
    private int $age;
    private string $curriculum;

    private const BASE_STORAGE_DIRECTORY = './students/';

    public function __construct(int $id, string $name = null, string $surname = null, int $age = null, string $curriculum = null)
    {
        $this->id = $id;
        $this->name = $name;
        $this->surname = $surname;
        $this->age = $age;
        $this->curriculum = $curriculum;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string|null $name
     */
    public function setName(?string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return string|null
     */
    public function getSurname(): ?string
    {
        return $this->surname;
    }

    /**
     * @param string|null $surname
     */
    public function setSurname(?string $surname): void
    {
        $this->surname = $surname;
    }

    /**
     * @return int|null
     */
    public function getAge(): ?int
    {
        return $this->age;
    }

    /**
     * @param int|null $age
     */
    public function setAge(?int $age): void
    {
        $this->age = $age;
    }

    /**
     * @return string|null
     */
    public function getCurriculum(): ?string
    {
        return $this->curriculum;
    }

    /**
     * @param string|null $curriculum
     */
    public function setCurriculum(?string $curriculum): void
    {
        $this->curriculum = $curriculum;
    }

    /**
     * @throws Exception
     */
    public function create(): string
    {
        $studentDetails = [
            'id' => $this->id,
            'name' => $this->name,
            'surname' => $this->surname,
            'age' => $this->age,
            'curriculum' => $this->curriculum,
        ];

        $errorMessage = 'ERROR: Duplicate student addition!';
        $successMessage = 'SUCCESS: Student added!';

        return $this->createStudentRecord($studentDetails, $errorMessage, $successMessage);
    }

    /**
     * @throws Exception
     */
    public function delete(): string
    {
        $dir = self::BASE_STORAGE_DIRECTORY.substr($this->id, 0, 2);
        try {
            if (is_dir($dir)) {

                array_map('unlink', glob("$dir/*.*"));
                if (!rmdir($dir)) {
                    return 'ERROR: Failed to delete student!';
                }

            } else {

                return 'ERROR: Student with ID: '. $this->id . ' does not exist!';
            }
        } catch (Exception $e)
        {
            throw new Exception($e->getMessage());
        }

        return 'SUCCESS: Student deleted!';
    }

    /**
     * @throws Exception
     */
    public function edit(string $name, string $surname, int $age, string $curriculum): string
    {
        $studentDetails = [
            'id' => $this->id,
            'name' => $name,
            'surname' => $surname,
            'age' => $age,
            'curriculum' => $curriculum,
        ];

        $directoryName = substr($this->id, 0, 2);
        $dir = self::BASE_STORAGE_DIRECTORY.$directoryName;
        $errorMessage = 'ERROR: Student details not updated!';
        $successMessage = 'SUCCESS: Student details updated!';

        try {
            if (is_dir($dir)) {
                $objects = scandir($dir);
                foreach ($objects as $object) {
                    if ($object != "." && $object != "..") {
                        if (filetype($dir."/".$object) == "dir")
                            rrmdir($dir."/".$object);
                        else unlink   ($dir."/".$object);
                    }
                }
                reset($objects);
                rmdir($dir);

            } else {

                return 'ERROR: Student does not exist!';
            }

            return $this->createStudentRecord($studentDetails, $errorMessage, $successMessage);
        } catch (Exception $e) {

            throw new Exception($e->getMessage());
        }
    }

    /**
     * @throws Exception
     */
    public function getDetails(): array
    {
        $dir = self::BASE_STORAGE_DIRECTORY.substr($this->id, 0, 2);
        try {
            if (is_dir($dir)) {

                return json_decode(file_get_contents($dir."/$this->id.json"), true);

            } else {

                throw new Exception('ERROR: Student with ID: '. $this->id . ' does not exist!');
            }
        } catch (Exception $e)
        {
            throw new Exception($e->getMessage());
        }
    }

    /**
     * @throws Exception
     */
    public function searchStudents(string $searchCriteria = '')
    {
        try {
            $students = [];
            $studentDirectories = glob(self::BASE_STORAGE_DIRECTORY.'*', GLOB_ONLYDIR);
            $numberOfStudents = false !== $studentDirectories ? count($studentDirectories) : 0;
            if (!empty($numberOfStudents)) {

                if (0 === strlen($searchCriteria)) {
                    for ($x = 0; $x < $numberOfStudents; $x++) {
                        $studentFiles = glob($studentDirectories[$x].'/*.json');
                        $students[$x] = json_decode(file_get_contents($studentFiles[0]), true);
                    }

                } else {

                    $allowedSearchFields = ['name', 'surname', 'age', 'curriculum'];
                    $searchCriteriaArray = explode('=', $searchCriteria);
                    $searchCriteria = $searchCriteriaArray[0];
                    $searchValue = $searchCriteriaArray[01];

                    if (!in_array($searchCriteria, $allowedSearchFields)) {
                        throw new Exception('Invalid search criteria used. Please search by the following: name or surname, or age, or curriculum');
                    }

                    for ($x = 0; $x < $numberOfStudents; $x++) {
                        $studentFiles = glob($studentDirectories[$x].'/*.json');
                        $student = json_decode(file_get_contents($studentFiles[0]), true);
                        switch (strtolower($searchCriteria)) {
                            case 'name':
                                if (strtolower($student['name']) === strtolower($searchValue)) {
                                    array_push($students, $student);
                                }
                                break;
                            case 'surname':
                                if (strtolower($student['surname']) === strtolower($searchValue)) {
                                    array_push($students, $student);
                                }
                                break;
                            case 'age':
                                if (strtolower($student['age']) === strtolower($searchValue)) {
                                    array_push($students, $student);
                                }
                                break;
                            case 'curriculum':
                                if (strtolower($student['curriculum']) === strtolower($searchValue)) {
                                    array_push($students, $student);
                                }
                                break;
                        }
                    }
                }

                return $students;
            }

            throw new Exception('No students loaded on the system.');

        } catch (Exception $e)
        {
            throw new Exception($e->getMessage());
        }
    }


    /**
     * @throws Exception
     */
    private function createStudentRecord(array $studentDetails, string $errorMessage, string $successMessage): string
    {
        $directoryName = substr($this->id, 0, 2);
        $dir = self::BASE_STORAGE_DIRECTORY.$directoryName;

        try {
            if (!is_dir($dir)) {
                mkdir($dir, 0777, true);
                chown($dir, get_current_user());
                $studentFile = fopen($dir."/$this->id.json", "w") or die ("Unable to write student file!");
                fwrite($studentFile, json_encode($studentDetails));
                fclose($studentFile);
            } else {

                return $errorMessage;
            }
        } catch (Exception $e) {

            throw new Exception($e->getMessage());
        }

        return $successMessage;
    }
}