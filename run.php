<?php
/*
 * This file is part of the Bidvest Assessment.
 *
 * @author     Koketso Mabuela <glenton92@gmail.com>
 */

require_once 'classes/Prompt.php';
require_once 'classes/Student.php';

$options = getopt('', [
    'action:',
    'id::',
    'help'
]);

if (isset($options['help'])) {
    sendUsageGuideline();
}

if (!isset($options['action'])) {
    die("You need to specify an action, with --action. Please execute the <php run.php --help> for further guidelines on usage.". PHP_EOL);
}
echo '=============================================================='.PHP_EOL;
echo 'Welcome to the Bidvest Student Management console application'.PHP_EOL;
echo '=============================================================='.PHP_EOL;
echo Prompt::promptExitMessage();
echo '=============================================================='.PHP_EOL;
echo PHP_EOL;
echo PHP_EOL;

switch ($options['action'])
{
    case 'add':
        echo add().PHP_EOL;
        break;
    case 'edit':
        print_r(edit($options));
        break;
    case 'delete':
        echo delete($options).PHP_EOL;
        break;
    case 'search':
        echo search($options);
        break;
    default:
        sendUsageGuideline();
        break;
}

//-------------------
// Add option
//-------------------
/**
 * @throws Exception
 */
function add(): string
{
    echo Prompt::promptIdMessage();
    $idInput = trim(readInput());

    if (0 === strlen($idInput)) {
        die('ERROR: ID is required');
    }

    if (strlen($idInput) < 7 && ctype_digit($idInput)) {
        die('ERROR: ID should be 7 digits long');
    }

    echo Prompt::promptNameMessage();
    $nameInput = trim(readInput());

    if (0 === strlen($nameInput)) {
        die('ERROR: Name is required');
    }

    echo Prompt::promptSurnameMessage();
    $surnameInput = trim(readInput());

    if (0 === strlen($surnameInput)) {
        die('ERROR: Surname is required');
    }

    echo Prompt::promptAgeMessage();
    $ageInput = trim(readInput());

    if (0 === strlen($ageInput)) {
        die('ERROR: Age is required');
    }

    echo Prompt::promptCurriculumMessage();
    $curriculumInput = trim(readInput());

    if (0 === strlen($curriculumInput)) {
        die('ERROR: Curriculum is required');
    }

    $studentObj = new Student((int)$idInput, $nameInput, $surnameInput, (int)$ageInput, $curriculumInput);

    return $studentObj->create().PHP_EOL;
}

//-------------------
// Edit option
//-------------------
/**
 * @throws Exception
 */
function edit(array $options)
{
    if (isset($options['id'])) {
        $studentObj = new Student((int)$options['id'], '', '', 0, '');

        $studentDetails = $studentObj->getDetails();

        $studentObj->setId($studentDetails["id"]);
        $studentObj->setName($studentDetails["name"]);
        $studentObj->setSurname($studentDetails["surname"]);
        $studentObj->setAge($studentDetails["age"]);
        $studentObj->setCurriculum($studentDetails["curriculum"]);

        echo "Leave field blank to keep previous value".PHP_EOL;

        echo Prompt::promptNameMessage($studentObj->getName());
        $nameInput = trim(readInput());
        $name = !empty($nameInput) ? $nameInput : $studentObj->getName();

        echo Prompt::promptSurnameMessage($studentObj->getSurname());
        $surnameInput = trim(readInput());
        $surname = !empty($surnameInput) ? $surnameInput : $studentObj->getSurname();

        echo Prompt::promptAgeMessage($studentObj->getAge());
        $ageInput = trim(readInput());
        $age = !empty($ageInput) ? $ageInput : $studentObj->getAge();

        echo Prompt::promptCurriculumMessage($studentObj->getCurriculum());
        $curriculumInput = trim(readInput());
        $curriculum = !empty($curriculumInput) ? $curriculumInput : $studentObj->getCurriculum();

        return $studentObj->edit($name, $surname, (int)$age, $curriculum).PHP_EOL;

    } else {

        die("You need to specify a student ID, e.g. --id=12345. In order to use the edit action". PHP_EOL);
    }
}

//-------------------
// Delete option
//-------------------
/**
 * @throws Exception
 */
function delete(array $options)
{
    if (isset($options['id'])) {
        $studentObj = new Student((int)$options['id'], '', '', 0, '');

        return $studentObj->delete().PHP_EOL;

    } else {

        die("You need to specify a student ID, e.g. --id=12345. In order to use the delete action". PHP_EOL);
    }
}

//-------------------
// Delete option
//-------------------
/**
 * @throws Exception
 */
function search(array $options)
{
    echo Prompt::promptSearchMessage().PHP_EOL;
    $searchCriteria = trim(readInput());
    $studentObj = new Student(0, '', '', 0, '');
    $studentDisplayTable = "Student Number\t\t\tName\t\t\tSurname\t\t\tAge\t\t\tCurriculum\t\t\t".PHP_EOL;
    $students = $studentObj->searchStudents($searchCriteria);

    foreach ($students as $student) {
        $studentDisplayTable .= $student['id']."\t\t\t".$student['name']."\t\t\t".$student['surname']."\t\t\t".$student['age']."\t\t\t".$student['curriculum']."\t\t\t".PHP_EOL;
    }

    return $studentDisplayTable;
}

//------------------
// Read input
//------------------
function readInput(): string
{
    $handle = fopen("php://stdin","r");

    return fgets($handle);
}


//-------------------------
// Generic usage guideline
//-------------------------
function sendUsageGuideline(): void
{
    die(
        'Usage:'.PHP_EOL.
        PHP_EOL.
        '--action=add' ."\t\t\t".  ' Add a new student'.PHP_EOL.
        '--action=edit --id=12344555' ."\t".  ' Edit details of an existing student with student ID: 12344555'.PHP_EOL.
        '--action=delete --id=12344555' ."\t".  ' Delete details of an existing student with student ID: 12344555'.PHP_EOL.
        '--action=search' ."\t\t\t".  ' Search student records loaded onto the system'.PHP_EOL
    );
}